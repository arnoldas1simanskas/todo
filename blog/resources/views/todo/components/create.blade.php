<div class="form-group">
    <form method="post" action="{{ route('todo.store') }}">
        @csrf
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <input type="text" required name="title" class="form-control @error('title') is-invalid @enderror" value="{{ old('title') }}"
                           placeholder="Enter task title">
                    @error('title')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="col-md-3">
                <input type="submit" value="Add task" class="btn-success btn btn-block">
            </div>
        </div>
    </form>
</div>