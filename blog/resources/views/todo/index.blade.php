@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1>
                    <a href="{{ route('todo.index') }}" style="text-decoration: none; color: black;">Todo list</a>
                </h1>
            </div>
        </div>
        {{-- Including todo create form--}}
        @include('todo.components.create')

        <table class="table table-striped">
            <thead>
            <tr>
                <th>Id</th>
                <th>Task title</th>
                <th>Created at</th>
                <th>Updated at</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>

            @foreach($todoItems as $item)
                <tr>
                    <td>
                        {{$item->id}}
                    </td>
                    <td>
                        {{ $item->title }}
                    </td>
                    <td>
                        {{ $item->created_at }}
                    </td>
                    <td>
                        {{ $item->updated_at }}
                    </td>
                    <td>
                        <form method="get" action="{{ route('todo.edit', [$item->id]) }}">
                            <input type="submit" value="Edit" class="btn btn-success">
                        </form>
                    </td>
                    <td>
                        <form method="post" action="{{ route('todo.destroy', [$item->id]) }}">
                            @csrf
                            @method('delete')
                            <input type="submit" value="X" class="btn btn-danger">
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $todoItems->links() }}

    </div>
@endsection