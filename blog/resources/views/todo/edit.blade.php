@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Updating: {{ $todo->title }}</h2>
                <form class="col-md-6" method="post" action="{{ route('todo.update', [$todo->id]) }}">
                    @csrf
                    @method('put')
                    <div>
                        <input class="form-control @error('title') is-invalid @enderror" type="text" name="title" value="{{ $todo->title }}">
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div>
                        <input class="btn btn-success" type="submit" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection